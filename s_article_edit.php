<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
		<link rel='stylesheet' href='css/layout.css'>
<style>
	form {
		width : 70%;
		margin-top : 2%;
		padding : 2%;
	}
	input {
		margin-bottom : 2%;
	}
	#titre {
		width : 70%;
	}
	#container_idarticle{
		display:none;
	}
	textarea {
		width : 100%;
		height : 20em;
		margin-bottom : 2%;
	}

</style>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
<?php
	if($_SESSION['user']->connected) {
		include("php/db_co.php");

		if(!isset($_GET["id"]) || empty($_GET["id"])) {
			echo "<a href='../blog.php'>_Error : no article was requested for edition.</a>";	
			exit;
		}
		$idarticle = $_GET["id"];

		$co = db_connect();

		$qresult = mysqli_query($co, "SELECT * FROM articles where 1=1 and id_article=$idarticle") or die("Error while fetching the articles " . mysqli_error($co));
	
		$data = mysqli_fetch_assoc($qresult);
		mysqli_free_result($qresult);

		if($data) {
			echo "
			<h2>Bienvenue, Zilot</h2>
			<h3>Édition d'un article</h3>
			<form method='post' action='php/article_editer.php'>
				<div>
					<label for='titre'>Titre : </label><input id='titre' name='titre' type='text' placeholder='titre' value='".$data['title']."' />
				</div>
				<div>
					<textarea id='contenu' name='contenu' >" . $data['content'] . "</textarea>
				</div>
				<div>
					<label for='visible'>visible : </label><input id='visible' name='visible' type='checkbox' " . ($data['visible'] ? 'checked' : '') ."/>
				</div>
				<div id='container_idarticle'>
					<input type='text' id='id_article' name='id_article' value='" . $data['id_article'] . "'/>
				</div>
				<div>
					<input type='submit' value='éditer'>
				</div>
			</form>

			";
		}

	}
	else {
		header("Location: https://patoeuf.fr/index.php");
	}
?>			
		</main>
		<footer>
		
		</footer>
	</body>
</html>
