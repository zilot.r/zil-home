<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
		<link rel='stylesheet' href='css/layout.css'>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
<?php
	if($_SESSION['user']->connected) {
		echo "
			<h3>Bienvenue, Zilot</h3>
			<a href='s_article_nouveau.php'>rédiger un nouvel article</a><br/>
			<a href='s_article_modif.php'>modifier un article</a><br/>
			________<br/><br/>
		";

	}
	else {
		header("Location: https://patoeuf.fr/index.php");
	}
?>			
		</main>
		<footer>
		
		</footer>
	</body>
</html>
