<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>

		<?php
			print_menu();
		?>
		<main>
			<p>Ce petit site perso vous présente les quelques projets sur lesquels j'ai pu travailler (pas tous). Certains sont en cours, je les rajouterai quand il y aura un peu de concret. Je tiendrai au courant  éventuellement sur la partie <a href="blog.php">_blog</a> si je n'ai pas la flemme.</p>
			<p>Je ne sais pas du tout si je vais entretenir ce site internet régulièrement, si je posterai des petits articles sur des trucs que je vois, remarque, qui m'énerve ou au couraire qui me plaisent.</p>
			<p>En tous cas, j'essairai de partager des musiques que j'aime bien sur la <a href="musique.php">_page musicale</a> ou encore des trucs qu'on m'a fait découvrir ou partagé.</p>
		</main>
		<footer>
		
		</footer>
	</body>
</html>
