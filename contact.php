<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
			<p><a href="https://matter.patoeuf.fr/signup_user_complete/?id=hq9ax91s178e3mpggtjc6q8w5r" target="_blank">_Discutons !</a></p>
			<p>Vous n'avez pas besoin de fournir une vrai adresse de couriel, il n'y a pas de vérification.</p>
			<p>Les mots de passe sont chiffrés, personne ne pourra les lire.</p>
			<p>Si vous perdez votre mot de passe, vous devrez recréer un compte ou me demander de reset le mot de passe</p>
		</main>
		<footer>
		
		</footer>
	</body>
</html>
