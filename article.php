<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
		<link rel='stylesheet' href='css/article.css'>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
<?php
	include("php/db_co.php");

	if(!isset($_GET["id"]) || empty($_GET["id"])) {
		echo "<a href='../blog.php'>_Error : no article was requested.</a>";	
		exit;
	}
	$idarticle = $_GET["id"];

	$co = db_connect();

	$qresult = mysqli_query($co, "SELECT * FROM articles where 1=1 and id_article = $idarticle and visible=1") 
			or die("Error while fetching the articles " . mysqli_error($co));
	$darticle = mysqli_fetch_assoc($qresult);
	mysqli_free_result($qresult);


	if($darticle) {
		$qresult = mysqli_query($co, "SELECT r.* FROM link_ressources lr, ressources r WHERE 1=1 and lr.id_article = $idarticle and lr.id_ressource = r.id_ressource order by r.id_ressource ASC") or die("Error while fetching the articles" . mysqli_error($co));

		$i = 0;
		$article_content = $darticle["content"];
		while($dressources = mysqli_fetch_assoc($qresult)) {
			$i++;
			$article_content = str_replace("[img$i]", "<div class='image'><img src='".str_replace("'", "\\'", $dressources["path"])."'/><div class='image_title'>".$dressources["name"]."</div></div>" , $article_content);
			$article_content = str_replace("[audio$i]", "<div class='audio'><div class='audio_title'>".$dressources["name"]."</div><audio controls src=\"".$dressources["path"]."\">Your browser does not support the <code>audio</code> element.</audio></div>", $article_content);
		}

		mysqli_free_result($qresult);

			echo "
				<h3>" . $darticle['title'] . "</h3>
				<div>
				" . $article_content . "
				</div>";
	}
	else {
		echo "<a href='../blog.php'>_Error : Article requested do not exists.</a>";	
	}

	mysqli_close($co);
?>
		</main>
		<footer>
		
		</footer>
	</body>
</html>
