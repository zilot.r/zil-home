<?php


	include("db_co.php");
	
	# check all data is received
	if(!isset($_POST["titre"])) {
		echo "<a href='../secret.php'>_Error : data not sent.</a>";	
		exit;
	}
	if(empty($_POST["titre"])) {
		echo "<a href='../secret.php'>_Error : no title</a>";
		exit;
	}
	if(empty($_POST["contenu"])) {
		echo "<a href='../secret.php'>_Error : no content</a>";
		exit;
	}
	if(empty($_POST["type"])) {
		echo "<a href='../secret.php'>_Error : no type</a>";
		exit;
	}
	
	$co = db_connect();
	# escape string
	$titre = mysqli_real_escape_string($co, $_POST["titre"]);
	$contenu = mysqli_real_escape_string($co, $_POST["contenu"]);
	if($_POST["visible"] == NULL) {
		$visible = 0;
	}
	else {
		$visible = 1;
	}
	$type = $_POST["type"];


	// insert article in db
	mysqli_query($co, "INSERT INTO articles(title, content, visible, type) VALUES('$titre', '$contenu', '$visible', '$type');") 
				or die ("Error while inserting article to db : " . mysqli_error($co));

	// retrieve article id + what will be the ressource(s) id
	$qresult = mysqli_query($co, "select max(id_article) as id_article from articles;")
			or die ("Error while getting article id : " . mysqli_error($co));
	$article = mysqli_fetch_assoc($qresult);
	$id_article = $article["id_article"] ? $article["id_article"] : 0;
	mysqli_free_result($qresult);

	if(!empty($_FILES["files"]["tmp_name"])) {
		$qresult = mysqli_query($co, "select max(id_ressource) as id_ressource from ressources;")
				or die ("Error while getting last ressource id : " . mysqli_error($co));
		$ressource = mysqli_fetch_assoc($qresult);
		$id_ressource = $ressource["id_ressource"] ? $ressource["id_ressource"] : 0;
		mysqli_free_result($qresult);
		
		// create dedicated dir for article
		if (!file_exists("../ressources/$id_article")) {
			if(mkdir("../ressources/$id_article", 0775)) {
			} 
			else {
				echo "failed to create directory ../ressources/$id_article ; check directory ownership and rights<br/> ";
			}
		}
		else {
			echo "Error : imposible to create directory.";
			exit;
		}

		// upload each files to the directory and link them to the article in db
		$uploads_dir = "ressources/$id_article/";
		$i=0;
		foreach ($_FILES["files"]["error"] as $key => $error) {
			$id_ressource++;

			if ($error == UPLOAD_ERR_OK) {
				$tmp_name = $_FILES["files"]["tmp_name"][$key];

				$name = basename($_FILES["files"]["name"][$key]);
				move_uploaded_file($tmp_name, "../$uploads_dir$name");
				$name = mysqli_real_escape_string($co, $name);

				mysqli_query($co, "INSERT INTO ressources(id_ressource, path, name) VALUES($i, '$uploads_dir$name', '$name');") 
					or die ("Error while inserting ressource to db. article : " . $id_article . " | name : $name | mysqli_error : " . mysqli_error($co));

				mysqli_query($co, "INSERT INTO link_ressources(id_article, id_ressource) VALUES($id_article, $id_ressource);") 
					or die ("Error while inserting link_ressource to db. article : " . $id_article . " | name : $name | mysqli_error : " . mysqli_error($co));

				echo "file uploaded successfuly<br/>";
			}
			else {
				echo "Error while uploading file. <br/>";
			}
		}

	}
	echo "article upload success<br/>";

	mysqli_close($co);

	header("Location: https://patoeuf.fr/s_admin.php");
?>




