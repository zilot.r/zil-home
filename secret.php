<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
<style>
	form {
		text-align : center;
	}
	div {
		margin-bottom : 15px;
	}
	input {
		background-color : #111;
		color : #22F;
		border : 1px solid #333;
	}
</style>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
<?php

	if($_SESSION['user']->connected) {
		header("Location: https://patoeuf.fr/s_admin.php");
	}
	else {
		echo "
			<form method='post' action='s_login.php'>
				<div>
					<label for='username'>username :</label>
					<input type='text' id='username' name='username' placeholder='username'/>
				</div>
				<div>
				<label for='password'>password :</label>
				<input type='password' id='password' name='password' placeholder='password'/>
				</div>
				<input type='submit' value='connection' />
			</form>
		";
	}
?>
		</main>
		<footer>
		
		</footer>
	</body>
</html>
