<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
		<link rel='stylesheet' href='css/layout.css'>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
<?php
	if($_SESSION['user']->connected) {
		include("php/db_co.php");

		$co = db_connect();

		$qresult = mysqli_query($co, "SELECT * FROM articles where 1=1") or die("Error while fetching the articles " . mysqli_error($co));

		echo "<ul>";
		while($data = mysqli_fetch_assoc($qresult)) {
			echo "
					<li><a href='s_article_edit.php?id=" . $data['id_article'] . "'>[" . $data['pubdate'] . "] - " . $data['title'] . "</a></li>";
		}
		echo "</ul>";
		mysqli_free_result($qresult);
		mysqli_close($co);

	}
	else {
		header("Location: https://patoeuf.fr/index.php");
	}
?>			
		</main>
		<footer>
		
		</footer>
	</body>
</html>
