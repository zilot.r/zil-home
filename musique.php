<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
		<link rel='stylesheet' href='css/blog.css'>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
			<ul>
<?php
	include("php/db_co.php");

	$co = db_connect();

	$qresult = mysqli_query($co, "SELECT * FROM articles where 1=1 and visible = 1 and type='MUSIC' ORDER BY pubdate DESC") or die("Error while fetching the articles " . mysqli_error($co));

	while($data = mysqli_fetch_assoc($qresult)) {
		echo "
				<li><a href='article.php?id=" . $data['id_article'] . "'>[" . $data['pubdate'] . "] - " . $data['title'] . "</a></li>";
	}
	mysqli_free_result($qresult);
	mysqli_close($co);
?>
			</ul>
		</main>
		<footer>
		
		</footer>
	</body>
</html>
