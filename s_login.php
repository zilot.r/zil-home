<!DOCTYPE html>
<html>
	<head>
		<?php
		include('php/layout.php');

		print_head();
		?>
		<style>
			main {
				text-align : center;	
			}
		</style>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>

<?php


	include("php/db_co.php");
	
	# check all data is received
	if(!isset($_POST["username"])) {
		echo "<a href='../secret.php'>_Error : data not sent.</a>";	
		exit;
	}
	if(empty($_POST["username"])) {
		echo "<a href='../secret.php'>_Error : no username</a>";
		exit;
	}
	if(empty($_POST["password"])) {
		echo "<a href='../secret.php'>_Error : no password</a>";
		exit;
	}

	
	$co = db_connect();

	# escape string
	$username = mysqli_real_escape_string($co, $_POST["username"]);
	$password = mysqli_real_escape_string($co, $_POST["password"]);

	# fetch db hash
	$qresult = mysqli_query($co, "SELECT password FROM admin WHERE username = '$username'") or die("Error while checking the password : " . mysqli_error($co));
	$db_hash = mysqli_fetch_assoc($qresult);
	mysqli_free_result($qresult);


	# verify hashes matches
	if(password_verify($password, $db_hash['password'])) {
		$options = array('cost' => 12);
		if(password_needs_rehash($db_hash['password'], PASSWORD_DEFAULT, $options)) {
			$newHash = password_hash($password, PASSWORD_DEFAULT, $options);
			mysqli_query($co, "UPDATE admin set password='$newHash' where username = '$username'") or die ("Unable to execute query : Can't update password : " . mysqli_error($co));
		}

		$_SESSION['user']->username = $username;
		$_SESSION['user']->connected = True;
		header("Location: https://patoeuf.fr/s_admin.php");
	}
	else {
		echo "<a href='../secret.php'>_Error : password is wrong.</a>";
	}

	mysqli_close($co);

?>

		</main>
		<footer>
		
		</footer>
	</body>
</html>
