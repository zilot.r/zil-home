<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");

		print_head();
		?>
		<link rel='stylesheet' href='css/layout.css'>
<style>
	form {
		width : 70%;
		margin-top : 2%;
		padding : 2%;
	}
	input {
		margin-bottom : 2%;
	}
	input[type=text] {
		width : 70%;
	}
	textarea {
		width : 100%;
		height : 20em;
		margin-bottom : 2%;
	}

</style>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
<?php
	if($_SESSION['user']->connected) {
		echo "
			<h2>Bienvenue, Zilot</h2>
			<h3>Ajout d'un nouvel article</h3>
			<form method='post' action='php/article_publier.php' enctype='multipart/form-data'>
				<div>
					<label for='titre'>Titre : </label><input id='titre' name='titre' type='text' placeholder='titre' />
				</div>
				<div>
					<textarea id='contenu' name='contenu' ></textarea>
				</div>
				<div>
					<label for='visible'>visible : </label><input id='visible' name='visible' type='checkbox' />
				</div>
				<div>
					<select id='type' name='type'>
						<option value='ARTICLE'>ARTICLE</option>
						<option value='MUSIC'>MUSIC</option>
						<option value='PHOTO'>PHOTO</option>
					</select>
				</div>
				<div>
					<label for='files'>Songs : </label>
					<input type='file' id='files' name='files[]' multiple />
				</div>
				<div>
					<input type='submit' value='publier'>
				</div>
			</form>

		";

	}
	else {
		header("Location: https://patoeuf.fr/index.php");
	}
?>			
		</main>
		<footer>
		
		</footer>
	</body>
</html>
