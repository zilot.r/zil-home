<!DOCTYPE html>
<html>
	<head>
		<?php
		include("php/layout.php");
		
		print_head();
		?>

		<link rel='stylesheet' href='css/projets.css'>
	</head>
	
	<body>
		<header>
		<?php
			print_title();
		?>
		</header>
		<?php
			print_menu();
		?>
		<main>
			<div>
				<h3>Discourtois</h3>
				<p>Discourtois est un robot <i>discord</i> créé en python pour aider à administrer un serveur discord. Il s'occupe, en se basant sur une liste noire, de supprimer les messages utilisant un vocabulaire non admis sur le serveur ; il renvoie le message complet à l'utilisateur fautif afin qu'il puisse reposter sans grossierté.</p>
				<p>Le code source est disponible ici : <a href="https://gitlab.com/afterthinking/discourtois">_https://gitlab.com/afterthinking/discourtois</a></p>
			</div>
			<div>
				<h3>Biblia</h3>
				<p>Biblia est un robot <i>discord</i> créé en python permettant de gérer une bibliothèque de livre numérique. Chaque utilisateur peur entrer un livre dont la fiche sera mise en forme par le robot.</p>
				<p>Le code source est disponible ici, mais une refonte du code est nécessaire : <a href="https://gitlab.com/techunit/community/babelia">_https://gitlab.com/techunit/community/babelia</a></p>
			</div>
			<div>
				<h3>arcenciel</h3>
				<p>arcenciel est un programme écrit en python permettant de gérer la couleur des leds d'une souris via l'outil ratbagctl. La lumière est choisie selon le pourcentage de RAM utilisée. Un script bash aide à l'installation de l'outil pour les utilisateurs de systemd.</p>
				<p>Le code source est disponible ici : <a href="https://gitlab.com/zilot.r/arcenciel">_https://gitlab.com/zilot.r/arcenciel</a></p>
			</div>
			<div>
				<h3>Site de soutient pour Julian Assange</h3>
				<p>À l'approche de son procès, un site internet de soutient fut créé avec quelques personnes sensibilisées à la liberté de la presse.</p>
				<p>Le code source est disponible ici : <a href="https://gitlab.com/techunit/apa/jawebsite">_https://gitlab.com/techunit/apa/jawebsite</a></p>
				<p>Le site internet est (encore) disponible ici : <a href="https://journalistepourlapaix.fr/">_https://journalistepourlapaix.fr/</a></p>
			</div>
			<div>
				<h3>Snake</h3>
				<p>Un petit projet très simple recréant un snake en langage C via la bibliothèque SDL. Via la ligne de commande on peut décider de la taille du terrain de jeu.</p>
				<p>Le code source est disponible ici : <a href="https://gitlab.com/zilot.r/csnake">_https://gitlab.com/zilot.r/csnake</a></p>
			</div>

		</main>
		<footer>
		
		</footer>
	</body>
</html>
